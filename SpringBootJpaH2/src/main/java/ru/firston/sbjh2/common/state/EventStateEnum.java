package ru.firston.sbjh2.common.state;

import org.apache.commons.lang3.StringUtils;
import ru.firston.sbjh2.entity.Event;

public enum EventStateEnum implements StateEnum<Event>{

    NULL("null"),
    NEW("new"),
    REGISTRATED("registrated"),
    PROCESS("process"),
    COMPLITED("complited"),
    COMPLITED_ERROR("complited_error");

    private String code;

    EventStateEnum(String code){
        this.code = code;
    }

    public final static EventStateEnum findByCode(String code) {

        if(StringUtils.isEmpty(code)) return EventStateEnum.NULL;

        for(EventStateEnum stateEnum: EventStateEnum.values())
          if(stateEnum.getCode().equals(code)) return stateEnum;

        return EventStateEnum.NULL;
    }

    @Override
    public String getCode() {
        return this.code;
    }
}
