package ru.firston.sbjh2.common;

import ru.firston.sbjh2.common.dto.ListWithTotalInfo;

import java.util.List;

/**
 * Anton Arefyev
 * anthon.arefyev@gmail.com
 *
 * @version 0.0.1
 */
public interface CustomResponseBuilder {

    @FunctionalInterface
    interface TransformList<T, K>{
        List<T> transform(List<K> list);
    }

    default <T, K> ListWithTotalInfo<T> responseListWithTotalInfo(
            List<K> data, int totalPage, long totalRows,
            TransformList<T, K> executor) {

        return ListWithTotalInfo.<T>builder()
                .list(executor.transform(data))
                .totalPages(totalPage)
                .totalRows(totalRows)
                .build();
    }
}
