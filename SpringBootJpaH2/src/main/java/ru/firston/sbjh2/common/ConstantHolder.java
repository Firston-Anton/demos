package ru.firston.sbjh2.common;

public final class ConstantHolder {

    public interface Persistence {

        interface Table {

            String EVENT = "EVENT";
            String DETAIL = "DETAIL";
            String STATE = "STATE";
            String USER = "USER";
            String HISTORY = "HISTORY";
        }
    }
    public interface Swagger {

        interface HttpStateCode {

            int SC_OK = 200;
            int SC_NO_CONTENT = 204;
            int SC_BAD_REQUEST = 400;
        }
    }
}
