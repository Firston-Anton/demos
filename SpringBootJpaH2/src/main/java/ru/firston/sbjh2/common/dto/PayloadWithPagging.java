package ru.firston.sbjh2.common.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Anton Arefyev
 * anthon.arefyev@gmail.com
 *
 * @version 0.0.1
 */
public abstract class PayloadWithPagging implements Serializable {

    private static final long serialVersionUID = -2213494946880380839L;

    @Getter
    @Setter
    private Pagging pagging;
}
