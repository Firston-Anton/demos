package ru.firston.sbjh2.common.state;

/**
 * Anton Arefyev
 * anthon.arefyev@gmail.com
 *
 * @version 0.0.1
 */
public interface StateModel<T extends StateEnum> {

    T getInitialState();

    T getStateEnum();

    boolean checkState(T state);
}
