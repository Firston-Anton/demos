package ru.firston.sbjh2.common.state;

public enum EventDetailStateEnum {

    NULL("null"),
    ACTUAL("actual"),
    NOT_ACTUAL("notActual");

    private String code;

    EventDetailStateEnum(String code){
        this.code = code;
    }
}
