package ru.firston.sbjh2.common.dto;

import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;
import java.util.List;

/**
 * Anton Arefyev
 * anthon.arefyev@gmail.com
 *
 * @version 0.0.1
 */
@Builder
public class ListWithTotalInfo<T> implements Serializable {

    private static final long serialVersionUID = -8703483657260615846L;

    /**
     * Подмножество выбранных записей
     */
    @Getter
    private List<T> list;

    /**
     * Общее количество записей. Семантика параматра  отличается от list.size()
     */
    @Getter
    private long totalRows;

    /**
     * Общее количество записей. Семантика параматра  отличается от list.size()
     */
    @Getter
    private int totalPages;


}
