package ru.firston.sbjh2.common.entity;

import ru.firston.common.entity.AbstractEntityCodeNameId;
import ru.firston.sbjh2.common.state.StateEnum;
import ru.firston.sbjh2.common.state.StateModel;

import javax.persistence.MappedSuperclass;

/**
 * Anton Arefyev
 * anthon.arefyev@gmail.com
 *
 * @version 0.0.1
 */
@MappedSuperclass
public abstract class AbstractEntityLogableModel<S extends StateEnum>
        extends AbstractEntityCodeNameId
        implements StateModel<S> {

    private static final long serialVersionUID = -4929689953698493881L;
}
