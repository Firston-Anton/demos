package ru.firston.sbjh2.common.dto;

import lombok.Getter;

import java.io.Serializable;

/**
 * Anton Arefyev
 * anthon.arefyev@gmail.com
 *
 * @version 0.0.1
 */
public class Pagging implements Serializable {

    private static final long serialVersionUID = 5945011781511999627L;

    @Getter
    private int page;

    @Getter
    private int rows;

    public Pagging(){
        // Значения по умолчанию
        this.page = 1; // первая страница
        this.rows = 1; // одна запись
    }

    public Pagging(int page, int rows){
        this.page = page;
        this.rows = rows;
    }
}
