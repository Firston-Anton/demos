package ru.firston.sbjh2.exception;

import javax.validation.constraints.NotNull;

/**
 * Anton Arefyev
 * anthon.arefyev@gmail.com
 *
 * @version 0.0.1
 */
public class DetailNotFoundException extends EntityObjectException {

    private static final long serialVersionUID = 834394257601438593L;

    public DetailNotFoundException(@NotNull Long id){
        super(String.format("Детали события с id: %s не найдено", id));
    }
}
