package ru.firston.sbjh2.exception;

public class EntityObjectException extends Exception {

    private static final long serialVersionUID = -3233412530662095923L;

    public EntityObjectException(String message){
        super(message);
    }

    public EntityObjectException(String message, Throwable e){
        super(message, e);
    }
}
