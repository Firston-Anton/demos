package ru.firston.sbjh2.exception;

public class EventFoundMoreOneException extends EntityObjectException {

    public EventFoundMoreOneException(String code){
        super(String.format("Найдено более одного события с кодом: %s", code));
    }
}
