package ru.firston.sbjh2.exception;

import lombok.extern.java.Log;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.nio.charset.Charset;

/**
 * Anton Arefyev
 * anthon.arefyev@gmail.com
 *
 * @version 0.0.1
 */
@Log
@RestControllerAdvice
public class RestControllerExceptionHandler {

    @ExceptionHandler(ResourceException.class)
    public ResponseEntity handlerException(ResourceException e){
        log.fine(e.getMessage());
        return ResponseEntity.status(e.getHttpStatus())
                .contentType(new MediaType(MediaType.TEXT_PLAIN, Charset.defaultCharset()))
                .body(e.getMessage());
    }
}
