package ru.firston.sbjh2.exception;

import javax.validation.constraints.NotNull;

public class EventNotFoundException extends EntityObjectException {

    public EventNotFoundException(@NotNull String code){
        super(String.format("Событие с кодом: %s не найдено", code));
    }

    public EventNotFoundException(@NotNull Long id){
        super(String.format("Событие с id: %s не найдено", id));
    }
}
