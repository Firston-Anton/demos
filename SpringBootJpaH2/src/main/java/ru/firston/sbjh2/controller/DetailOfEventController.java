package ru.firston.sbjh2.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ru.firston.sbjh2.Tags;
import ru.firston.sbjh2.common.CustomResponseBuilder;
import ru.firston.sbjh2.entity.Detail;
import ru.firston.sbjh2.exception.EntityObjectException;
import ru.firston.sbjh2.exception.EventNotFoundException;
import ru.firston.sbjh2.exception.ResourceException;
import ru.firston.sbjh2.model.DetailRequest;
import ru.firston.sbjh2.model.DetailResponse;
import ru.firston.sbjh2.service.EventService;

import java.util.List;
import java.util.stream.Collectors;

import static ru.firston.sbjh2.common.ConstantHolder.Swagger.HttpStateCode.*;

/**
 * Anton Arefyev
 * anthon.arefyev@gmail.com
 *
 * @version 0.0.1
 */
@Api(
    value = "Controller for details of the events",
    tags = {
      Tags.DETAIL
    },
    description = "Api by details of the events"
)
@RestController
@RequestMapping("/event/{code}/detail")
public class DetailOfEventController implements CustomResponseBuilder {

    private final EventService eventService;

    @Autowired
    public DetailOfEventController(EventService eventService){
        this.eventService = eventService;
    }

    @ApiOperation(
        value = "Список всех имеющихся в системе пользователей",
        notes = "Не имеет дополнительных параметров или ограничений",
        tags = {
            Tags.DETAIL
        },
        response = List.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = "Successfully retrieved list of the event`s detail"),
            @ApiResponse(code = SC_BAD_REQUEST, message = "Error in the input parameters of the request")
    })
    @GetMapping("/list")
    public List<DetailResponse> getDetailOfEvent(@PathVariable String code){

        List<Detail> details = null;
        try{
            details = eventService.getDetailsByEventCode(code);
        }catch (EventNotFoundException e) {
            throw new ResourceException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

        return details.stream().map(detail -> {
            return DetailResponse.builder()
                    .id(detail.getId())
                    .description(detail.getDescription())
                    .build();
        }).collect(Collectors.toList());
    }

    @ApiOperation(
        value = "Сохранение детализации события",
        tags = {
            Tags.DETAIL
        },
        response = DetailResponse.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = "Successfully saved event`s detail"),
            @ApiResponse(code = SC_BAD_REQUEST, message = "Error in the input parameters of the request")
    })
    @PostMapping
    public DetailResponse saveDetailOfEvent(@PathVariable String code,
                                            @RequestBody DetailRequest request){

        Detail detail = null;
        Long id = request.getId();
        String desc = request.getDescription();
        try{
            detail = id != null ? eventService.saveDetailOfEvent(desc, id, null)
                                : eventService.saveDetailOfEvent(desc, code);
        } catch (EntityObjectException e){
            throw new ResourceException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

        return DetailResponse.builder()
                .id(detail.getId())
                .modified(detail.getModified())
                .build();
    }
}
