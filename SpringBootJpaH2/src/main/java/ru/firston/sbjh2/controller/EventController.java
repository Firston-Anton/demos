package ru.firston.sbjh2.controller;

import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.firston.sbjh2.Tags;
import ru.firston.sbjh2.common.CustomResponseBuilder;
import ru.firston.sbjh2.common.dto.ListWithTotalInfo;
import ru.firston.sbjh2.model.EventRequestList;
import ru.firston.sbjh2.model.EventResponseShort;
import ru.firston.sbjh2.model.EventRequest;
import ru.firston.sbjh2.model.EventResponse;
import ru.firston.sbjh2.entity.Event;
import ru.firston.sbjh2.exception.EventNotFoundException;
import ru.firston.sbjh2.service.EventService;

import java.util.ArrayList;
import java.util.List;

import static ru.firston.sbjh2.common.ConstantHolder.Swagger.HttpStateCode.*;

@Api(
    value = "Controller for events",
    tags = {
      Tags.EVENT
    },
    description = "Api by events"
)
@RestController
@RequestMapping("/event")
public class EventController implements CustomResponseBuilder {

    private final EventService eventService;

    @Autowired
    public EventController(EventService eventService){
        this.eventService = eventService;
    }

    @ApiOperation(
        value = "Список всех имеющихся в системе событий",
        notes = "Не имеет дополнительных параметров или ограничений",
        tags = {
            Tags.EVENT
        },
        response = List.class
    )
    @ApiResponses(value = {
        @ApiResponse(code = SC_OK, message = "Successfully retrieved list of the events")
    })
    @GetMapping("/list")
    public List<EventResponseShort> getEvents(){

        List<EventResponseShort> result = new ArrayList<>();
        eventService.getAllEvents().forEach(
            event -> result.add(
                EventResponseShort.builder().build().builder()
                .code(event.getCode())
                .name(event.getName())
                .date(event.getDate())
                .build()
            )
        );
        return result;
    }

    @ApiOperation(
        value = "Список событий с возможностью фильтрации, пагинации, сортировки",
        tags = {
            Tags.EVENT
        },
        response = List.class
    )
    @ApiResponses(value = {
        @ApiResponse(code = SC_OK, message = "Successfully retrieved list of the events")
    })
    @PostMapping("/list-by-criteria")
    public ListWithTotalInfo<EventResponseShort> getEventsByCriteria(@RequestBody EventRequestList request){

        ListWithTotalInfo<Event> entityEvents = eventService.getEventsByPayload(request);
        return responseListWithTotalInfo(
            entityEvents.getList(),
            entityEvents.getTotalPages(),
            entityEvents.getTotalRows(),
            data -> {
                List<EventResponseShort> result = new ArrayList<>();
                data.forEach(item -> {
                    result.add(
                        EventResponseShort.builder()
                        .code(item.getCode())
                        .name(item.getName())
                        .date(item.getDate()).build()
                    );
                });
                return result;
            }
        );
    }

    @ApiOperation(
        value = "Возвращает код нового события",
        tags = {
            Tags.EVENT
        },
        response = EventResponse.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = "Successfully retrieved new event")
    })
    @GetMapping("/new")
    public EventResponse newEvent(){
        Event event = eventService.newEvent();
        return EventResponse.builder().code(event.getCode()).moment(event.getDate()).build();
    }

    @ApiOperation(
        value = "Получение события по коду",
        tags = {
            Tags.EVENT
        },
        response = EventResponse.class
    )
    @ApiResponses(value = {
        @ApiResponse(code = SC_OK, message = "Successfully retrieved event by code")
    })
    @GetMapping("/{code}")
    public EventResponse getEvent(@PathVariable String code){

        Event event = null;
        try{
            event = eventService.getEventByCode(code);
        } catch (EventNotFoundException e){
            e.printStackTrace();
        }
        return EventResponse.builder()
            .id(event.getId())
            .code(event.getCode())
            .name(event.getName())
            .moment(event.getDate())
            .note(event.getNote())
            .build();
    }

    @ApiOperation(
        value = "Сохранение изменений события",
        notes = "Если запрос выполнить с несуществующим кодом, будет сгенерировано новое событие с произвольных кодом",
        tags = {
            Tags.EVENT
        },
        response = EventResponse.class
    )
    @ApiResponses(value = {
        @ApiResponse(code = SC_OK, message = "Successfully retrieved saved event")
    })
    @PostMapping
    public EventResponse saveEvent(@RequestBody EventRequest request){

        Event event = eventService.saveEvent(request.getCode(), request.getName(), request.getDate(), request.getNote());
        return EventResponse.builder()
                .code(event.getCode())
                .moment(event.getDate())
                .build();
    }

    @ApiOperation(
        value = "Удаление события по коду",
        tags = {
            Tags.EVENT
        },
        response = Void.class
    )
    @ApiResponses(value = {
        @ApiResponse(code = SC_NO_CONTENT, message = "Successfully deleted event")
    })
    @DeleteMapping("/{code}")
    public ResponseEntity<Void> deleteEvent(@PathVariable String code){

        eventService.deleteEvent(code);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }
}
