package ru.firston.sbjh2.controller;

/**
 * Anton Arefyev
 * anthon.arefyev@gmail.com
 *
 * @version 0.0.1
 */
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.firston.sbjh2.Tags;
import ru.firston.sbjh2.model.UserResponseShort;

import java.util.ArrayList;
import java.util.List;

import static ru.firston.sbjh2.common.ConstantHolder.Swagger.HttpStateCode.*;

@Api(
    value = "Controller for users",
    tags = {
        Tags.USER
    },
    description = "Api by users"
)
@RestController
@RequestMapping("/user")
public class UserController {

    @ApiOperation(
        value = "Список всех имеющихся в системе пользователей",
        notes = "Не имеет дополнительных параметров или ограничений",
        tags = {
            Tags.USER
        },
        response = List.class
    )
    @ApiResponses(value = {
        @ApiResponse(code = SC_OK, message = "Successfully retrieved list of the users")
    })
    @GetMapping("/list")
    public List<UserResponseShort> getUsers(){

        List<UserResponseShort> result = new ArrayList<>();
        return result;
    }
}
