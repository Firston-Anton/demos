package ru.firston.sbjh2.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.firston.sbjh2.Tags;
import ru.firston.sbjh2.model.HistoryResponse;

import java.util.ArrayList;
import java.util.List;

import static ru.firston.sbjh2.common.ConstantHolder.Swagger.HttpStateCode.SC_OK;

/**
 * Anton Arefyev
 * anthon.arefyev@gmail.com
 *
 * @version 0.0.1
 */
@Api(
    value = "Controller for history",
    tags = {
        Tags.HISTORY
    },
    description = "Api by history change"
)
@RestController
@RequestMapping("/history")
public class HistoryController {

    @ApiOperation(
        value = "история всех изменений",
        notes = "Не имеет дополнительных параметров или ограничений",
        tags = {
            Tags.HISTORY
        },
        response = List.class
    )
    @ApiResponses(value = {
            @ApiResponse(code = SC_OK, message = "Successfully retrieved list of the users")
    })
    @GetMapping("/list")
    public List<HistoryResponse> getHistories(){

        List<HistoryResponse> result = new ArrayList<>();
        return result;
    }
}
