package ru.firston.sbjh2.model;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAttribute;
import java.io.Serializable;
import java.time.LocalDateTime;

public class EventRequest implements Serializable {

    private static final long serialVersionUID = -1031272730336614876L;

    interface Model{
        String NAME = "Event";
        interface Fields {
            String CODE = "code";
            String NAME = "name";
            String DATE = "date";
            String NOTE = "note";
        }
    }

    @Getter
    @Setter
    @XmlAttribute(name = Model.Fields.CODE, required = true)
    protected String code;

    @Getter
    @Setter
    @XmlAttribute(name = Model.Fields.NAME, required = true)
    protected String name;

    @Getter
    @Setter
    @XmlAttribute(name = Model.Fields.DATE, required = true)
    protected LocalDateTime date;

    @Getter
    @Setter
    @XmlAttribute(name = Model.Fields.NOTE, required = false)
    protected String note;
}
