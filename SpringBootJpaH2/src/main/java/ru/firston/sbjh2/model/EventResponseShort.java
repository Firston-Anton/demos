package ru.firston.sbjh2.model;

import lombok.*;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Anton Arefyev
 * anthon.arefyev@gmail.com
 *
 * @version 0.0.1
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement
@XmlType(name = EventResponseShort.Model.NAME)
public class EventResponseShort implements Serializable {

    private static final long serialVersionUID = -9020092745276570771L;

    interface Model{
        String NAME = "EventResponseShort";
        interface Fields {
            String CODE = "code";
            String NAME = "name";
            String DATE = "date";
        }
    }

    @Getter
    @Setter
    @XmlAttribute(name = Model.Fields.CODE, required = true)
    protected String code;

    @Getter
    @Setter
    @XmlAttribute(name = Model.Fields.NAME, required = true)
    protected String name;

    @Getter
    @Setter
    @XmlAttribute(name = Model.Fields.DATE, required = true)
    protected LocalDateTime date;
}
