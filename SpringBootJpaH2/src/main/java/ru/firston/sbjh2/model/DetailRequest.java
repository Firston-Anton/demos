package ru.firston.sbjh2.model;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAttribute;
import java.io.Serializable;

/**
 * Anton Arefyev
 * anthon.arefyev@gmail.com
 *
 * @version 0.0.1
 */
public class DetailRequest implements Serializable {

    private static final long serialVersionUID = -3017166235077300550L;

    interface Model{
        String NAME = "DetailRequest";
        interface Fields {
            String ID = "id";
            String DESCRIPTION = "description";
        }
    }

    @Getter
    @Setter
    @XmlAttribute(name = Model.Fields.ID, required = false)
    protected Long id;

    @Getter
    @Setter
    @XmlAttribute(name = Model.Fields.DESCRIPTION, required = true)
    protected String description;
}
