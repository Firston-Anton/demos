package ru.firston.sbjh2.model;

import lombok.Getter;
import lombok.Setter;
import ru.firston.sbjh2.common.dto.PayloadWithPagging;

/**
 * Anton Arefyev
 * anthon.arefyev@gmail.com
 *
 * @version 0.0.1
 */
public class EventRequestList extends PayloadWithPagging {

    private static final long serialVersionUID = 3365061172412539970L;

    @Getter
    @Setter
    private String name;
}
