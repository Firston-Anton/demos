package ru.firston.sbjh2.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;

@Builder
public class EventResponse implements Serializable {

    private static final long serialVersionUID = -5976458423331746503L;

    interface Model{
        String NAME = "EventResponse";
        interface Fields {
            String ID = "id";
            String CODE = "code";
            String NAME = "name";
            String MOMENT = "moment";
            String NOTE = "note";
        }
    }

    @Getter
    @Setter
    protected Long id;

    @Getter
    @Setter
    protected String code;

    @Getter
    @Setter
    protected String name;

    @Getter
    @Setter
    protected LocalDateTime moment;

    @Getter
    @Setter
    protected String note;
}
