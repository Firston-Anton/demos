package ru.firston.sbjh2.model;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import ru.firston.sbjh2.entity.Detail;

import javax.persistence.Column;
import javax.validation.constraints.PastOrPresent;
import javax.xml.bind.annotation.XmlAttribute;
import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * Anton Arefyev
 * anthon.arefyev@gmail.com
 *
 * @version 0.0.1
 */
@Builder
public class DetailResponse implements Serializable {

    private static final long serialVersionUID = 5584990240282910293L;

    interface Model{
        String NAME = "DetailResponse";
        interface Fields {
            String ID = "id";
            String DESCRIPTION = "descroption";
            String MODIFIED = "modified";
        }
    }

    @Getter
    @Setter
    @XmlAttribute(name = Model.Fields.ID, required = true)
    protected Long id;

    @Getter
    @Setter
    @XmlAttribute(name = Model.Fields.DESCRIPTION, required = true)
    protected String description;

    @Getter
    @Setter
    @PastOrPresent
    @XmlAttribute(name = Model.Fields.MODIFIED, required = true)
    private ZonedDateTime modified;
}
