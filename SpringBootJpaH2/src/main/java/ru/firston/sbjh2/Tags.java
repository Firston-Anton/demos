package ru.firston.sbjh2;

/**
 * Anton Arefyev
 * anthon.arefyev@gmail.com
 *
 * @version 0.0.1
 */
public interface Tags {

    String EVENT = "Event";
    String DETAIL = "Details of the events";
    String USER = "Info by users";
    String HISTORY = "History";
}
