package ru.firston.sbjh2.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.firston.sbjh2.entity.State;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@Repository
public interface StateDAO extends CrudRepository<State, Long> {

    Optional<State> findByCode(@NotNull String code);

    Iterable<State> findAllByType(@NotNull String type);
}
