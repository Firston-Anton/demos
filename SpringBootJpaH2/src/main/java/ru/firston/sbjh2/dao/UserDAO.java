package ru.firston.sbjh2.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.firston.sbjh2.entity.User;

@Repository
public interface UserDAO extends CrudRepository<User, Long> {

    String USER_DAO = "userDAO";

}
