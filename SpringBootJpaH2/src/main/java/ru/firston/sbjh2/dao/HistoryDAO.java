package ru.firston.sbjh2.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.firston.sbjh2.entity.History;
import ru.firston.sbjh2.entity.unique.MomentObjectId;

@Repository
public interface HistoryDAO extends CrudRepository<History, MomentObjectId> {

}
