package ru.firston.sbjh2.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.firston.sbjh2.entity.Detail;

@Repository
public interface DetailDAO extends CrudRepository<Detail, Long> {

}
