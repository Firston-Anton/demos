package ru.firston.sbjh2.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.firston.sbjh2.entity.Event;

import javax.validation.constraints.NotNull;
import java.util.Optional;

@Repository
public interface EventDAO extends PagingAndSortingRepository<Event, Long> {

    Optional<Event> findByCode(@NotNull String code);

    @Override
    @Transactional(timeout = 5)
    Iterable<Event> findAll();

    @Transactional(readOnly = true, timeout = 5)
    @Query("select e from Event e where e.name like %:name%")
    Page<Event> findAllByNameContaining(String name, Pageable pageable);
}
