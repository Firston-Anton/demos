package ru.firston.sbjh2.service;

import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.bean.ColumnPositionMappingStrategy;
import au.com.bytecode.opencsv.bean.CsvToBean;
import lombok.extern.java.Log;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;

import java.io.FileReader;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;
import java.util.stream.Collectors;

/**
 * Anton Arefyev
 * anthon.arefyev@gmail.com
 *
 * @version 0.0.1
 */
@Service
@Log
public class FileServiceImpl implements FileService {

    @Override
    @SuppressWarnings({"rawtypes", "unchecked"})
    public <T> List<T> readCSV(String path, ColumnPositionMappingStrategy<T> strategy) {

        CsvToBean<T> csv = new CsvToBean<>();
        List<?> list;
        try(FileReader fr = new FileReader(getClass().getResource(path).getFile())){
            CSVReader csvReader = new CSVReader(fr);
            list = csv.parse(strategy, csvReader);
        }catch (Exception e){
            log.log(Level.SEVERE, "Ошибка чтения файла", e);
            return Collections.emptyList();
        }
        Class<T> type = strategy.getType();
        return CollectionUtils.isEmpty(list) ? Collections.emptyList()
                                             : list.stream().map(type::cast).collect(Collectors.toList());
    }

    @Override
    @SuppressWarnings({"rawtypes", "unchecked"})
    public <T> ColumnPositionMappingStrategy<T> setColumMapping(Class<T> cls, String[] columns) {

        ColumnPositionMappingStrategy strategy = new ColumnPositionMappingStrategy();
        strategy.setType(cls);
        strategy.setColumnMapping(columns);
        return strategy;
    }
}
