package ru.firston.sbjh2.service;

import ru.firston.sbjh2.common.state.StateEnum;
import ru.firston.sbjh2.entity.State;

/**
 * Anton Arefyev
 * anthon.arefyev@gmail.com
 *
 * @version 0.0.1
 */
public interface StateService {

   State getStateByEnum(StateEnum stateEnum);
}
