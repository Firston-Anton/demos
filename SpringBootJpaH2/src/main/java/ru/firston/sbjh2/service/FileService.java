package ru.firston.sbjh2.service;

import au.com.bytecode.opencsv.bean.ColumnPositionMappingStrategy;

import java.util.List;

/**
 * Anton Arefyev
 * anthon.arefyev@gmail.com
 *
 * @version 0.0.1
 */
public interface FileService {

    /**
     * Чтение csv файла и преобразование в коллекцию java объектов
     * @param path
     * @param strategy
     * @param <T>
     * @return
     */
    <T> List<T> readCSV(String path, ColumnPositionMappingStrategy<T> strategy);

    /**
     * Создание стратегии связки между java bean и колонками csv file
     * @param cls - класс java bean
     * @param columns - колонки (важна последовательность, как в csv файле)
     * @param <T>
     * @return
     */
    <T> ColumnPositionMappingStrategy<T> setColumMapping(Class<T> cls, String[] columns);
}
