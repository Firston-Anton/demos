package ru.firston.sbjh2.service;

import lombok.extern.java.Log;
import org.apache.commons.collections4.IterableUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.firston.sbjh2.common.dto.ListWithTotalInfo;
import ru.firston.sbjh2.common.dto.Pagging;
import ru.firston.sbjh2.common.state.EventDetailStateEnum;
import ru.firston.sbjh2.common.state.EventStateEnum;
import ru.firston.sbjh2.dao.DetailDAO;
import ru.firston.sbjh2.dao.EventDAO;
import ru.firston.sbjh2.entity.Detail;
import ru.firston.sbjh2.entity.Event;
import ru.firston.sbjh2.exception.DetailNotFoundException;
import ru.firston.sbjh2.exception.EventNotFoundException;
import ru.firston.sbjh2.model.EventRequestList;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Log
@Service
public class EventServiceImpl implements EventService {

    private final EventDAO eventDAO;

    private final DetailDAO detailDAO;

    private final StateService stateService;

    private final LogService logService;

    @Autowired
    EventServiceImpl(EventDAO eventDAO, DetailDAO detailDAO, StateService stateService, LogService logService){
        this.eventDAO = eventDAO;
        this.detailDAO = detailDAO;
        this.stateService = stateService;
        this.logService = logService;
    }

    @Override
    public Event newEvent() {

        Event event = new Event();
        EventStateEnum stateEnum = event.getInitialState();
        event.setCode(RandomStringUtils.randomAlphabetic(5));
        event.setDate(ZonedDateTime.now().toLocalDateTime());
        event.setState(stateService.getStateByEnum(stateEnum));
        event = eventDAO.save(event);
        logService.write(event, stateEnum, String.format("Созднано новое событие с кодом: %s.",  event.getCode()));
        return  event;
    }

    @Override
    public Event getEventById(@NotNull Long id) throws EventNotFoundException{

        Optional<Event> op = eventDAO.findById(id);
        if(op.isPresent()) return op.get();
        throw new EventNotFoundException(id);
    }

    @Override
    public Event getEventByCode(@NotNull String code) throws EventNotFoundException {

        Optional<Event> op = eventDAO.findByCode(code);
        if(op.isPresent()) return op.get();
        throw new EventNotFoundException(code);
    }

    @Override
    public List<Event> getAllEvents() {
        return IterableUtils.toList(eventDAO.findAll());
    }

    @Override
    public ListWithTotalInfo<Event> getEventsByPayload(EventRequestList payload) {

        Pagging pagging = payload.getPagging();
        Page<Event> pageEvents =  eventDAO.findAllByNameContaining(
                payload.getName(),
                PageRequest.of(pagging.getPage(), pagging.getRows())
        );

        return ListWithTotalInfo.<Event>builder()
                .list(pageEvents.getContent())
                .totalPages(pageEvents.getTotalPages())
                .totalRows(pageEvents.getTotalElements())
                .build();
    }

    @Override
    public Event saveEvent(@NotNull String code, @NotNull String name, @NotNull LocalDateTime date, String note){

        Event event;
        try{
            event = getEventByCode(code);
        }catch (EventNotFoundException e){
            log.warning(String.format("События c кодом: %s не найдено. Создание нового события", code));
            event = newEvent();
        }

        logService.write(event, event.getStateEnum(), String.format(
            "Изменение параметров. name: %s -> %s, note: %s -> %s", event.getName(), name, event.getNote(), note
        ));
        event.setName(name);
        event.setNote(note);
        event.setDate(date);
        eventDAO.save(event);
        logService.write(event, event.getStateEnum(), "Параметр: name успешно изменен");
        return event;
    }

    @Override
    public void deleteEvent(@NotNull String code) {

        try{
            eventDAO.deleteById(getEventByCode(code).getId());
            log.info(String.format("События с кодом: %s  успешно удалено", code));
        }catch (EventNotFoundException e){
            log.warning(String.format("События с кодом: %s  не существует", code));
        }
    }

    @Override
    public Detail getDetailbyId(@NotNull Long id) throws DetailNotFoundException{

        Optional<Detail> op = detailDAO.findById(id);
        if(op.isPresent()) return op.get();
        throw new DetailNotFoundException(id);
    }

    @Transactional(propagation = Propagation.REQUIRED)
    @Override
    public List<Detail> getDetailsByEventCode(@NotNull String code) throws EventNotFoundException{
        return new ArrayList<>(getEventByCode(code).getDetailSet());
    }

    @Override
    public Detail saveDetailOfEvent(@NotNull String descroption, @NotNull String eventCode) throws EventNotFoundException {

        Detail detail = new Detail();
        detail.setDescription(descroption);
        detail.setCreated(ZonedDateTime.now());
        detail.setEvent(getEventByCode(eventCode));
        return detailDAO.save(detail);
    }

    @Override
    public Detail saveDetailOfEvent(@NotNull String descroption, @NotNull Long id,
                                    EventDetailStateEnum state) throws DetailNotFoundException {

        Detail detail = getDetailbyId(id);
        detail.setDescription(descroption);
        detail.setModified(ZonedDateTime.now());
        return detailDAO.save(detail);
    }
}
