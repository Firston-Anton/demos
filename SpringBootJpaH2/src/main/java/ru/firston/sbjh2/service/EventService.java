package ru.firston.sbjh2.service;

import ru.firston.sbjh2.common.dto.ListWithTotalInfo;
import ru.firston.sbjh2.common.state.EventDetailStateEnum;
import ru.firston.sbjh2.entity.Detail;
import ru.firston.sbjh2.entity.Event;
import ru.firston.sbjh2.exception.DetailNotFoundException;
import ru.firston.sbjh2.exception.EventNotFoundException;
import ru.firston.sbjh2.model.EventRequestList;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

public interface EventService {

    /**
     * Создание нового события
     * @return
     */
    Event newEvent();

    /**
     *  Получение события по ID
     * @param id - ИД события
     * @return
     */
    Event getEventById(@NotNull Long id) throws EventNotFoundException;

    /**
     * Получение события по коду
     * @param code
     * @return
     */
    Event getEventByCode(@NotNull String code) throws EventNotFoundException;

    /**
     * Получение всех событий
     * @return
     */
    List<Event> getAllEvents();

    /**
     * Список событий по ограничениям
     * @param payload
     * @return
     */
    ListWithTotalInfo<Event> getEventsByPayload(EventRequestList payload);

    /**
     * Сохранение события
     * @param code
     * @param name
     * @param date
     * @param note
     * @return
     */
    Event saveEvent(@NotNull String code, @NotNull String name, @NotNull LocalDateTime date, String note);

    /**
     * Удаление события
     * @param code - Код события
     */
    void deleteEvent(@NotNull String code);

    /**
     * Получение детали события по ID
     * @param id
     * @return
     */
    Detail getDetailbyId(@NotNull Long id) throws DetailNotFoundException;
    /**
     * Детали события
     * @param code
     * @return
     */
    List<Detail> getDetailsByEventCode(@NotNull String code) throws EventNotFoundException;

    /**
     * Добавление описания для события
     * @param descroption
     * @param eventCode - код события
     * @return
     * @throws EventNotFoundException
     */
    Detail saveDetailOfEvent(@NotNull String descroption, @NotNull String eventCode) throws EventNotFoundException;

    /**
     * Изменение детали события
     * @param descroption
     * @param id
     * @return
     * @throws DetailNotFoundException
     */
    Detail saveDetailOfEvent(@NotNull String descroption, @NotNull Long id, EventDetailStateEnum state) throws DetailNotFoundException;
}
