package ru.firston.sbjh2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.firston.sbjh2.common.entity.AbstractEntityLogableModel;
import ru.firston.sbjh2.common.state.StateEnum;
import ru.firston.sbjh2.dao.HistoryDAO;
import ru.firston.sbjh2.entity.History;
import ru.firston.sbjh2.entity.unique.MomentObjectId;

import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

/**
 * Anton Arefyev
 * anthon.arefyev@gmail.com
 *
 * @version 0.0.1
 */
@Service
public class LogServiceImpl implements LogService {

    private final StateService stateService;

    private final HistoryDAO historyDAO;

    @Autowired
    LogServiceImpl(StateService stateService, HistoryDAO historyDAO){
        this.stateService = stateService;
        this.historyDAO = historyDAO;
    }

    @Override
    public <T extends AbstractEntityLogableModel, S extends StateEnum> MomentObjectId write(
            @NotNull T entity, @NotNull S state, String description) {

        if(!entity.checkState(state)) throw new UnsupportedOperationException("несоответствие модели и статуса");

        History history = new History();
        history.setObjectId(entity.getId());
        history.setMoment(ZonedDateTime.now());
        history.setState(stateService.getStateByEnum(state));
        history.setDescription(description);
        historyDAO.save(history);

        return MomentObjectId.builder().objectId(history.getObjectId()).moment(history.getMoment()).build();
    }
}
