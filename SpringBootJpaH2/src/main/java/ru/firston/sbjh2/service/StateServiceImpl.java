package ru.firston.sbjh2.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.firston.sbjh2.common.state.StateEnum;
import ru.firston.sbjh2.dao.StateDAO;
import ru.firston.sbjh2.entity.State;

import java.util.Optional;

/**
 * Anton Arefyev
 * anthon.arefyev@gmail.com
 *
 * @version 0.0.1
 */
@Service
public class StateServiceImpl implements StateService {

    private final StateDAO stateDAO;

    @Autowired
    StateServiceImpl(StateDAO stateDAO){
        this.stateDAO = stateDAO;
    }

    @Override
    public State getStateByEnum(StateEnum stateEnum) {
        Optional<State> op = stateDAO.findByCode(stateEnum.getCode());
        if(op.isPresent()) return op.get();
        throw new UnsupportedOperationException(String.format("БД не поддерживает статус %s", stateEnum.getCode()));
    }
}
