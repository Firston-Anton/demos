package ru.firston.sbjh2.service;

import ru.firston.sbjh2.common.entity.AbstractEntityLogableModel;
import ru.firston.sbjh2.common.state.StateEnum;
import ru.firston.sbjh2.entity.unique.MomentObjectId;

import javax.validation.constraints.NotNull;

/**
 * Anton Arefyev
 * anthon.arefyev@gmail.com
 *
 * @version 0.0.1
 */
public interface LogService {

    <T extends AbstractEntityLogableModel, S extends StateEnum> MomentObjectId write(
            @NotNull T entity, @NotNull S state, String description);
}
