package ru.firston.sbjh2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(Java8DateTimeConfiguration.class)
public class SpringBootJpaH2Application {

    public static  void main(String... args){

        SpringApplication.run(SpringBootJpaH2Application.class, args);
    }
}
