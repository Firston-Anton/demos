package ru.firston.sbjh2.init;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import ru.firston.sbjh2.dao.StateDAO;
import ru.firston.sbjh2.entity.Event;
import ru.firston.sbjh2.entity.State;
import ru.firston.sbjh2.exception.EventNotFoundException;
import ru.firston.sbjh2.service.EventService;
import ru.firston.sbjh2.service.FileService;

import java.time.ZoneId;
import java.time.ZonedDateTime;

import static ru.firston.sbjh2.init.DemoURLHolder.*;

@Log
@Component
public class DataInit implements ApplicationRunner {

    private final EventService eventService;

    private final FileService fileService;

    private final StateDAO stateDAO;

    @Autowired
    public DataInit(EventService eventService, FileService fileService, StateDAO stateDAO){
        this.eventService = eventService;
        this.fileService = fileService;
        this.stateDAO = stateDAO;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {

        log.info("Создание справочника статусов");
        fillingTableState();
        log.info("Создание справочника статусов завершено");
        log.info("Создание событтий");
        fillingTableEvent();
        log.info("Создание событтий завершено");
        log.info("swagger ui: " + SWAGGER2_UI);
        log.info("swagger api doc: " + SWAGGER2_API_DOC);
        log.info("h2 console: " + H2_CONSOLE);
    }

    /**
     * Наполнение сущности "События"
     */
    private void fillingTableEvent(){

        // code не может быть пустым, отдельный вызов, выставлен заглушкой
        fillingTableDatail(
            eventService.saveEvent("1", "Новый Год",
                ZonedDateTime.of(
                        2019, 1, 1,0, 0,0,0, ZoneId.systemDefault()
                ).toLocalDateTime(),
                "Юху!"
            )
        );

        eventService.saveEvent("2", "Рождество",
                ZonedDateTime.of(
                        2019, 1, 7,0, 0,0,0, ZoneId.systemDefault()
                ).toLocalDateTime(),
                null
        );
        eventService.saveEvent("3", "Первый рабочий день",
                ZonedDateTime.of(
                        2019, 1, 9,0, 0,0,0, ZoneId.systemDefault()
                ).toLocalDateTime(),
                "Печалька"
        );
    };

    /**
     * Наполнение сущности "детали события"
     * @param event
     */
    private void fillingTableDatail(Event event){

        String eventCode = event.getCode();
        try{
            eventService.saveDetailOfEvent("Покупка подарков", eventCode);
            eventService.saveDetailOfEvent("Украшение ёлки", eventCode);
            eventService.saveDetailOfEvent("Запуск салюта", eventCode);
        } catch (EventNotFoundException e){
            // сюда не должно попасть
        }
    }

    /**
     * Наполнение сущности "Статусы"
     */
    private void fillingTableState(){

        stateDAO.saveAll(
            fileService.readCSV(
                CSV_STATES_DATA,
                fileService.setColumMapping(
                    State.class,
                    new String[]{"id", "code", "name", "type"}
                )
            )
        );
    }
}
