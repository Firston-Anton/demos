package ru.firston.sbjh2.init;

/**
 * Anton Arefyev
 * anthon.arefyev@gmail.com
 *
 * @version 0.0.1
 */
public final class DemoURLHolder {

    public static String SWAGGER2_UI = "http://localhost:8080/swagger-ui.html";
    public static String SWAGGER2_API_DOC = "http://localhost:8080/v2/api-docs";
    public static String H2_CONSOLE = "http://localhost:8080/h2-console";

    public static String CSV_STATES_DATA = "/data/dictionary_states.csv";
}
