package ru.firston.sbjh2.entity.unique;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.ZonedDateTime;

@Builder
@AllArgsConstructor
@NoArgsConstructor
public class MomentObjectId implements Serializable {

    private Long objectId;
    private ZonedDateTime moment;
}
