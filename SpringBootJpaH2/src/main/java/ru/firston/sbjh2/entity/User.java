package ru.firston.sbjh2.entity;

import lombok.Getter;
import lombok.Setter;
import ru.firston.common.entity.AbstractEntityCodeNameId;
import ru.firston.common.entity.AbstractEntityNameId;

import javax.persistence.*;

import static ru.firston.sbjh2.common.ConstantHolder.Persistence.Table.USER;

@Entity
@Table(name = USER)
public class User extends AbstractEntityNameId {

    private static final long serialVersionUID = -8101995659648921983L;

    public interface Model extends AbstractEntityCodeNameId.Model{

        String SURNAME = "surname";
        String PATRONYMIC = "patronymic";
        String LOGIN = "login";
    }

    public interface Table extends AbstractEntityCodeNameId.Table {

        String NAME = USER;
        interface Fields extends AbstractEntityCodeNameId.Table.Fields{

            String SURNAME = "SURNAME";
            String PATRONYMIC = "patronymic";
            String LOGIN = "LOGIN";
        }
    }

    @Getter
    @Setter
    @Column(name = Table.Fields.SURNAME, nullable = false, insertable = true, updatable = true)
    private String surname;

    @Getter
    @Setter
    @Column(name = Table.Fields.PATRONYMIC, nullable = true, insertable = true, updatable = true)
    private String patronymic;

    @Getter
    @Setter
    @Column(name = Table.Fields.LOGIN, nullable = true, insertable = true, updatable = false)
    private String login;
}
