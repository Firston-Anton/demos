package ru.firston.sbjh2.entity;

import lombok.Getter;
import lombok.Setter;
import ru.firston.common.entity.AbstractEntityId;

import javax.persistence.*;
import javax.validation.constraints.PastOrPresent;

import java.time.ZonedDateTime;

import static ru.firston.sbjh2.common.ConstantHolder.Persistence.Table.DETAIL;

@Entity
@Table(name = DETAIL)
public class Detail extends AbstractEntityId {

    private static final long serialVersionUID = 4327827102262384067L;

    public interface Model extends AbstractEntityId.Model{

        String CREATED = "created";
        String MODIFIED = "modified";
        String DESCRIPTION = "description";
        String STATE = "state";
        String EVENT = "event";
    }

    public interface Table extends AbstractEntityId.Table {

        String NAME = DETAIL;
        interface Fields extends AbstractEntityId.Table.Fields{

            String CREATED = "CREATED";
            String MODIFIED = "MODIFIED";
            String DESCRIPTION = "description";
            String STATE_ID = "STATE_ID";
            String EVENT_ID = "EVENT_ID";
        }
    }

    @Getter
    @Setter
    @JoinColumn(name = Table.Fields.STATE_ID)
    @ManyToOne(optional = true, fetch = FetchType.LAZY)
    private State state;

    @Getter
    @Setter
    @JoinColumn(name = Table.Fields.EVENT_ID)
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private Event event;

    @Getter
    @Setter
    @PastOrPresent
    @Column(name = Table.Fields.CREATED, nullable = false, insertable = true, updatable = false)
    private ZonedDateTime created;

    @Getter
    @Setter
    @PastOrPresent
    @Column(name = Table.Fields.MODIFIED, nullable = true, insertable = true, updatable = true)
    private ZonedDateTime modified;

    @Getter
    @Setter
    @Column(name = Table.Fields.DESCRIPTION, nullable = true, insertable = true, updatable = false)
    private String description;
}
