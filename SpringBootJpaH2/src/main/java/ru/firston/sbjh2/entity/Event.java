package ru.firston.sbjh2.entity;

import lombok.*;

import ru.firston.common.entity.AbstractEntityId;
import ru.firston.sbjh2.common.entity.AbstractEntityLogableModel;
import ru.firston.sbjh2.common.state.EventStateEnum;

import javax.persistence.*;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

import static ru.firston.sbjh2.common.ConstantHolder.Persistence.Table.EVENT;

@Entity
@Table(name = EVENT)
public class Event  extends AbstractEntityLogableModel<EventStateEnum> {

    private static final long serialVersionUID = 5822412475996745325L;

    public interface Model extends AbstractEntityId.Model{

        String DATE = "date";
        String NOTE = "note";
        String STATE = "state";
    }

    public interface Table extends AbstractEntityId.Table {

        String NAME = EVENT;
        interface Fields extends AbstractEntityId.Table.Fields{

            String DATE = "DATE";
            String NOTE = "NOTE";
            String STATE_ID = "STATE_ID";
        }
    }

    @Override
    public EventStateEnum getInitialState() {
        return EventStateEnum.NEW;
    }

    @Override
    public EventStateEnum getStateEnum() {
        return EventStateEnum.findByCode(this.getState().getCode());
    }

    @Override
    public boolean checkState(EventStateEnum state) {

        boolean checked = false;

        if(state.equals(EventStateEnum.NULL)) return checked;
        for(EventStateEnum stateEnum: EventStateEnum.values()){
            if(stateEnum.equals(state)){
                checked = true;
                break;
            }
        }

        return checked;
    }

    @Getter
    @Setter
    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    private State state;

    @Getter
    @Setter
    @OneToMany(mappedBy = Detail.Model.EVENT, fetch = FetchType.LAZY)
    private Set<Detail> detailSet = new HashSet<>();

    @Getter
    @Setter
    @Column(name = Table.Fields.DATE, nullable = false, insertable = true, updatable = true)
    private LocalDateTime date;

    @Getter
    @Setter
    @Column(name = Table.Fields.NOTE, nullable = true, insertable = true, updatable = true)
    private String note;
}
