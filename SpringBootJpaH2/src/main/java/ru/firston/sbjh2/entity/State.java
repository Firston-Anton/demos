package ru.firston.sbjh2.entity;

import lombok.Getter;
import lombok.Setter;
import ru.firston.common.entity.AbstractEntityCodeNameId;

import javax.persistence.*;

import java.util.Set;

import static ru.firston.sbjh2.common.ConstantHolder.Persistence.Table.STATE;

@Entity
@Table(name = STATE)
public class State extends AbstractEntityCodeNameId {

    private static final long serialVersionUID = 1614110880801221058L;

    public interface Model extends AbstractEntityCodeNameId.Model{

        String TYPE = "type";
    }

    public interface Table extends AbstractEntityCodeNameId.Table {

        String NAME = STATE;
        interface Fields extends AbstractEntityCodeNameId.Table.Fields{

            String TYPE = "TYPE";
        }
    }

    @Getter
    @Setter
    @Column(name = Table.Fields.TYPE, nullable = false, insertable = true, updatable = false)
    private String type;

    @Getter
    @Setter
    @OneToMany(mappedBy = Event.Model.STATE)
    private Set<Event> eventSet;

    @Getter
    @Setter
    @OneToMany(mappedBy = Detail.Model.STATE)
    private Set<Detail> detailSet;
}
