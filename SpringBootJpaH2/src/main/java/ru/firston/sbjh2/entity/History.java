package ru.firston.sbjh2.entity;

import lombok.Getter;
import lombok.Setter;
import ru.firston.sbjh2.entity.unique.MomentObjectId;

import javax.persistence.*;

import java.io.Serializable;
import java.time.ZonedDateTime;

import static ru.firston.sbjh2.common.ConstantHolder.Persistence.Table.HISTORY;

@Entity
@Table(name = HISTORY)
@IdClass(MomentObjectId.class)
public class History implements Serializable {

    private static final long serialVersionUID = -244558705918012266L;

    public interface Model{

        String MOMENT = "moment";
        String OBJECT_ID = "objectId";
        String STATE_ = "state";
        String USER = "user";
        String DESCRIPTION = "description";
    }

    public interface Table{

        String NAME = HISTORY;
        interface Fields{

            String MOMENT = "MOMENT";
            String OBJECT_ID = "OBJECT_ID";
            String STATE_ID = "STATE_ID";
            String USER_ID = "USER_ID";
            String DESCRIPTION = "DESCRIPTION";
        }
    }

    @Getter
    @Setter
    @JoinColumn(name = Table.Fields.STATE_ID)
    @ManyToOne(optional = true, fetch = FetchType.LAZY)
    private State state;


    @Getter
    @Setter
    @JoinColumn(name = Table.Fields.USER_ID)
    @ManyToOne(optional = true, fetch = FetchType.LAZY)
    private User user;

    @Id
    @Getter
    @Setter
    @Column(name = Table.Fields.MOMENT, nullable = false, insertable = true, updatable = false)
    private ZonedDateTime moment;

    @Id
    @Getter
    @Setter
    @Column(name = Table.Fields.OBJECT_ID, nullable = false, insertable = true, updatable = false)
    private Long objectId;

    @Getter
    @Setter
    @Column(name = Table.Fields.DESCRIPTION, nullable = true, insertable = true, updatable = false)
    private String description;
}
