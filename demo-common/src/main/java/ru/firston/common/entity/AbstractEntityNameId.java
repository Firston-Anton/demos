package ru.firston.common.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class AbstractEntityNameId  extends AbstractEntityId{

    private static final long serialVersionUID = -4356071199941106683L;

    public interface Model extends AbstractEntityId.Model{
        String NAME = "name";
    }

    public interface Table extends AbstractEntityId.Table {

        interface Fields extends AbstractEntityId.Table.Fields{
            String NAME = "NAME";
        }
    }

    @Getter
    @Setter
    @Column(name = Table.Fields.NAME, nullable = true, insertable = true, updatable = true)
    private String name;
}
