package ru.firston.common.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@MappedSuperclass
public abstract class AbstractEntityCodeNameId extends AbstractEntityNameId {

    private static final long serialVersionUID = -4172234791549313797L;

    public interface Model extends AbstractEntityNameId.Model{
        String CODE = "code";
    }

    public interface Table extends AbstractEntityNameId.Table {

        interface Fields extends AbstractEntityNameId.Table.Fields{
            String CODE = "CODE";
        }
    }

    @Getter
    @Setter
    @NotEmpty
    @NotBlank
    @Column(name = Table.Fields.CODE, nullable = false, insertable = true, updatable = true)
    private String code;
}
