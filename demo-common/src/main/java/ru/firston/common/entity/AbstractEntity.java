package ru.firston.common.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@MappedSuperclass
public abstract class AbstractEntity<T> implements Serializable {

    private static final long serialVersionUID = 4605009949098413888L;

    public interface Model{
        // describe entity model
        String IDENTIFIER = "identifier";
    }

    public interface Table {
        public String SCHEMA = "";
        static String NAME = "";

        interface Fields {
            String IDENTIFIER = "IDENTIFIER";
        }
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Getter
    @Setter
    @Column(name = Table.Fields.IDENTIFIER, nullable = false, updatable = false)
    protected T identifier;
}
