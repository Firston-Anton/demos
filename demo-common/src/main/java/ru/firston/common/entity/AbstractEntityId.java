package ru.firston.common.entity;

import javax.persistence.MappedSuperclass;

@MappedSuperclass
public abstract class AbstractEntityId extends AbstractEntity<Long> {

    private static final long serialVersionUID = 7155271830983863614L;

    public interface Model extends AbstractEntity.Model{
        String ID = "id";
    }

    public interface Table extends AbstractEntity.Table {

        interface Fields extends AbstractEntity.Table.Fields{
            String IDENTIFIER = "ID";
        }
    }

    public Long getId() {
        return identifier;
    }

    public void setId(Long id) {
        identifier = id;
    }
}
